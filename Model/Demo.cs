﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WpfDemo.Model;

public class Demo
{
    public int Value { get; set; } = 2;
    public ObservableCollection<int> AllValues { get; set; } = new ObservableCollection<int> { 1, 2 };
}
