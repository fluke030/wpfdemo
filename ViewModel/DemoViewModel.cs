﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using System.Collections.Generic;
using WpfDemo.Model;

namespace WpfDemo.ViewModel;

public class DemoViewModel : ObservableObject
{
    private Demo _demo;

    public DemoViewModel()
    {
        _demo = new Demo();
        IncrementCommand = new RelayCommand(DoIncrement);
    }

    public Demo Demo
    {
        get => _demo;
        set
        {
            SetProperty(ref _demo, value);
        }
    }

    public IRelayCommand IncrementCommand { get; }

    private void DoIncrement()
    {
        _demo.Value++;
        _demo.AllValues.Add(_demo.Value);
    }
}
